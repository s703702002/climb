import requests
import urllib
import time
import xml.etree.ElementTree as ET
from selenium import webdriver


chrome = webdriver.Chrome('./chromedriver')

def getUrl(url):
  print('request url is: ', urllib.parse.unquote(url))
  chrome.get(url)
  print('sleep 1 second')
  time.sleep(1)
  print('========================')

def getSiteMap():
  res = requests.get('https://tw.buy.yahoo.com/shopdaily/sitemap.xml')
  root = ET.fromstring(res.text)
  print('total clusters', len(root))
  for child in root:
    url = child[0].text
    getUrl(url)
  print('all url have been requested')
  chrome.quit()

getSiteMap()