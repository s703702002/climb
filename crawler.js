const fs = require('fs');
const path = require('path');
const request = require('request');
const cheerio = require('cheerio');

const sleep = (timeout) => 
    new Promise((resolve, reject) => {
      setTimeout(resolve, timeout)
    });

// 用img的name string來產生src
function unsuan(s) {
  const x = s.substring(s.length - 1);
  const w = "abcdefghijklmnopqrstuvwxyz";
  const xi = w.indexOf(x) + 1;
  const sk = s.substring(s.length - xi - 12, s.length - xi - 1);
  s = s.substring(0, s.length - xi - 12);
  const k = sk.substring(0, sk.length - 1);
  const f = sk.substring(sk.length - 1);
  for (i = 0; i < k.length; i++) {
      eval("s=s.replace(/" + k.substring(i, i + 1) + "/g,'" + i + "')")
  }
  const ss = s.split(f);
  s = "";
  for (i = 0; i < ss.length; i++) {
      s += String.fromCharCode(ss[i])
  }
  return s
};

function downloadImg(img_url, directoryName, pageIndex) {
  const extension = img_url.match(/\.(jpg|png)$/ig)[0].toLowerCase();
  request
  .get(img_url)
  .on('error', function(err) {
    console.log(err)
  })
  .pipe(fs.createWriteStream(`comics/${directoryName}/${pageIndex}${extension}`))
}

function getImageUrl(page_url, directoryName, callback) {
  return new Promise((resolve, reject) => {
    request(page_url, function(err, res, body) {
      const $ = cheerio.load(body);
      const now_count = $('#hdPageIndex')[0].attribs.value;
      const img_name = $('#iBodyQ img').attr('name');
      const img_url = `http://245.94201314.net/dm09/${unsuan(img_name)}`;
      callback(img_url, directoryName, now_count);
      resolve();
    })
  })
};

// calc how many pages in this chapter
function getPages(page_url) {
  return new Promise((resolve, reject) => {
    request(page_url, function(err, res, body) {
      const $ = cheerio.load(body);
      const total_count = $('#hdPageCount')[0].attribs.value;
      const page_lists = [...new Array(Number(total_count))].map((v, i) => i + 1);
      // 回傳這集總共有幾頁的array [1, 2, 3 ....]
      resolve(page_lists);
    });
  });
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

// 測試用 海賊王924集
// const testurl = 'http://dmeden.net/comichtml/328297/1.html?s=9';

// 海賊王首頁
const target_index_url = 'http://dmeden.net/comicinfo/170.html';

request(target_index_url, function(err, res, body) {
  const $ = cheerio.load(body);
  const total_ul = $('ul.cVolUl')['1'];
  // 最近一集的tag,ul > li > a
  const newest_a = total_ul.children['0'].children[0];
  const title = newest_a.attribs.title;
  const newest_num = Number(title.match(/(\d+)/g)[0]);

  console.log(`最新一集為: ${title}，將開始下載${newest_num - 4}集到${newest_num}集`);

  const download_lists = total_ul.children.slice(0, 1);

  asyncForEach(download_lists, async (li, index) => {
    const a = li.children[0];
    const {
      title: title_count,
      href,
    } = a.attribs;
    const ID = href.match(/ID=(\d+)&/)[1];
    const comic_url = `http://dmeden.net/comichtml/${ID}/1.html?s=9`;

    console.log(`start download ${title_count} after 1 second.`);

    const dir_path = './' + path.resolve(__dirname, '/comics', title_count);

    if (!fs.existsSync('./comics')) {
      fs.mkdirSync(path.resolve(__dirname, './comics'));
    }

    if (!fs.existsSync(dir_path)){
      fs.mkdirSync(dir_path);
    }

    // await sleep(1000);

    getPages(comic_url)
      .then((page_lists) => {
        asyncForEach(page_lists, async (pageIndex, index) => {
          // 間隔兩秒在抓
          // await sleep(2000);
          const page_url = `http://dmeden.net/comichtml/${ID}/${pageIndex}.html?s=9`;
          getImageUrl(page_url, title_count, downloadImg)
            .then(() => {
              console.log(title_count, pageIndex, '下載完成');
            })
        });
      });
  });
});