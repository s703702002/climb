import requests
from bs4 import BeautifulSoup
import re
import os

def get_pages(page_url):
  res = requests.get(page_url)
  soup = BeautifulSoup(res.text, 'html.parser')
  total_pages = soup.find(id = 'hdPageCount')['value']
  return int(total_pages)

def get_imageUrl(url):
  res = requests.get(url)
  soup = BeautifulSoup(res.text, 'html.parser')
  img_attr_name = soup.find(id="iBodyQ").find('img')['name']
  return unsuan(img_attr_name)

def download_img(url, directoryName, pageIndex):
  r = requests.get(url, allow_redirects=True)
  open('comics_py/{}/{}.jpg'.format(directoryName, pageIndex), 'wb').write(r.content)
  print('download finished')

def unsuan(s):
  _len = len(s)
  x = s[-1]
  w = "abcdefghijklmnopqrstuvwxyz"
  xi = w.find(x) + 1
  sk = s[_len - xi - 12: _len - xi - 1]
  s = s[0:_len - xi - 12]
  k = sk[0: len(sk) - 1]
  f = sk[-1]

  for i in range(0, len(k)):
    s = s.replace(k[i: i+1], str(i))
  ss = s.split(f)
  s = ""

  for i in range(0, len(ss)):
    s += chr(int(ss[i]))
  
  return s

# test_url = 'http://245.94201314.net/dm09//ok-comic01/h/hzw/act_920/z_0018_16100.JPG'

# 海賊王924畫第一頁
ur = 'http://dmeden.net/comichtml/328297/1.html?s=9'

# 海賊王首頁
target_index_url = 'http://dmeden.net/comicinfo/170.html'

res = requests.get(target_index_url)
soup = BeautifulSoup(res.text, 'html.parser')
all_ul = soup.find_all('ul', 'cVolUl')[1]
newest_title = all_ul.contents[0].contents[0]['title']
newest_num = int(newest_title[3:6])

print('最新一集為: {}，將開始下載{}集到{}集'.format(newest_title, newest_num-4, newest_num))

download_lists = all_ul.contents[0:1]
for li in download_lists:
  a = li.contents[0]
  title = a['title']
  href = a['href']
  ID = re.findall(r'ID=(\d+)&', href)[0]
  comic_url = 'http://dmeden.net/comichtml/{}/1.html?s=9'.format(ID)
  print('start download {} after 1 second'.format(title))

  if not os.path.isdir('./comics_py'):
    os.makedirs('./comics_py')

  if not os.path.isdir('./comics_py/{}'.format(title)):
    os.makedirs('./comics_py/{}'.format(title))

  pages = get_pages(comic_url)

  for page in range(1, pages+1):
    page_url = 'http://dmeden.net/comichtml/{}/{}.html?s=9'.format(ID,page)
    img_url = get_imageUrl(page_url)
    img_src = 'http://245.94201314.net/dm09/' + img_url
    download_img(img_src, title, page)
